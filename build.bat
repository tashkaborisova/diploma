@echo off

set FILE_VERSION=0.9
set PRODUCT_VERSION=0.9
set NW_VERSION=0.12.1
set DESCRIPTION=imtp-2
set COMPANY_NAME=Tashka Borisova Inc.
set COMMENTS=IMTP-2

cd %~dp0

mkdir tmp
xcopy /E /Y app\* tmp

cd tmp
..\build\7z a -tzip -mx0 ..\output\imtp-2.nw *
cd ..
rd /S /Q tmp

copy /B /Y build\nw.exe+output\imtp-2.nw output\tmp.exe 

taskkill /f /im imtp-2.exe
build\reshacker -modify output\tmp.exe, output\imtp-2.exe, app\icon.ico, icon, IDR_MAINFRAME,
del output\tmp.exe

build\verpatch output\imtp-2.exe /fn /high /vft2 -1 /langid 1049 /va %FILE_VERSION% /pv %PRODUCT_VERSION% /s AssemblyVersion %NW_VERSION% /s OriginalFilename nw.exe /s InternalName imtp-2-app /s ProductName IMTP-2 /s FileDescription "%DESCRIPTION%" /s LegalCopyright "Copyright (C) 2015 %COMPANY_NAME%" /s CompanyName "%COMPANY_NAME%" /s Comments "%COMMENTS%"
