/* Имитационное моделирование транспортных потоков в условиях городского многополосного движения */

/* include */
    var fs = require('fs'),
        path = require('path'),
        gui = require('nw.gui'),
        nwWindow = gui.Window.get();

/* libs */
    var Map = require('./script/lib/map').Map,
        Dialog = require('./script/lib/dialog').Dialog;

    var vector = require('./script/lib/vector');

/* parts */
    var draw = require('./script/main/draw'),
        vehicles = require('./script/main/vehicles'),
        stats = require('./script/main/stats');

/* app */
    var mapFile = process.argv[1] || 
            path.join(path.dirname(process.execPath), 'map.json');

    var spawnPeriodically = true,
        spawnFlood = false,
        showEverything = false,
        vehicleTypes = {},
        origins = {};

    function getType(){
        var value = 0.0;
        for (var type in vehicleTypes){
            value += vehicleTypes[type];
        }

        value *= Math.random();
        for (var type in vehicleTypes){
            value -= vehicleTypes[type];
            if (value < 0.0){
                break;
            }
        }

        return type;
    }

    fs.readFile(mapFile, function (error, data){
        if (error != null){
            return;
        }

        var map = window.map = Map.deserialize(data);
        draw.drawMap(map);
        draw.centerMap(map);
        draw.setupEvents();
        vehicles.init(map, stats)

        vehicles.origins.sort(function (a, b){
            return a.id - b.id;
        }).forEach(function (o){
            var saved = localStorage['origin' + o.id];
            origins[o.id] = saved != null ? +saved : 250;
            $('<label>#' + o.id + ' <input data-id="' + o.id + '" type="number" min="0" max="999" step="10" value="' + 
                    origins[o.id] + '"></label>').appendTo('#origins');
        });

        $('#origins input').change(function (){
            var id = this.getAttribute('data-id');
            localStorage['origin' + id] = origins[id] = +this.value || 0;
        });

        var started = Date.now(),
            calculationTime = 0,
            framesPerSecond = 0.0,
            frames = 0,
            pause = 0;

        /* обновление машин */
        setInterval(function (){
            if (!pause){
                var now = Date.now();
                vehicles.update();
                calculationTime += (Date.now() - now - calculationTime) / 20;
            }
        }, 20);

        /* автоспавн */
        setInterval(function (){
            if (!pause){
                if (spawnFlood){
                    for (var i = 0; i < 10; i++){
                        vehicles.add(getType());
                    }
                } else if (spawnPeriodically){
                    for (var n in origins){
                        if (Math.random() < origins[n] / 6e3){
                            vehicles.add(getType(), n);
                        }
                    }
                }
            }
        }, 10);

        /* подсчёт FPS */
        setInterval(function (){
            framesPerSecond += (frames / .1 - framesPerSecond) / 3;
            frames = 0;
        }, 100);

        /* отрисовка */
        var selected = null;
        function drawFrame(){
            frames++;

            if (selected && selected.step){
                draw.drawStatsVehicle(selected, pause);
            } else {
                var passed = (pause || Date.now()) - started;
                draw.drawStats(vehicles.count, passed, framesPerSecond, calculationTime);
            }

            draw.drawVehicles(vehicles.list, showEverything);
            window.requestAnimationFrame(drawFrame);
        }

        window.requestAnimationFrame(drawFrame);

        $('canvas').click(function (e){
            if (e.which == 1){
                var pos = draw.pageToMap(e.pageX, e.pageY),
                    list = vehicles.list;
                
                selected = null;
                for (var i = 0; i < list.length; i++){
                    if (vector.distanceSqr(list[i].pos, pos) < 12 * 12){
                        selected = list[i];
                        break;
                    }
                }
            }
        });

        $(window).keyup(function (e){
            if (e.keyCode == 32){
                if (pause){
                    var delta = Date.now() - pause,
                        list = vehicles.list;

                    for (var i = 0; i < list.length; i++){
                        list[i].meta.timestamp += delta;
                    }

                    started += delta;
                    pause = 0;
                } else {
                    pause = Date.now();
                }
            }
        });
    });

/* inputs */
    $('#vehicles input').each(function (){
        var type = this.getAttribute('data-type'),
            saved = localStorage['vehicle' + type];
        if (saved != null){
            this.value = saved;
        }
        vehicleTypes[type] = +this.value;
    }).change(function (){
        var type = this.getAttribute('data-type');
        localStorage['vehicle' + type] = vehicleTypes[type] = +this.value || 0;
    });

    $('#reset').click(function (){
        var dialog = new Dialog('Сброс', [
            '<p>Сбросить настройки и перезагрузить приложение?</p>'
        ], function (){
            localStorage.clear();
            nwWindow.reloadDev();
        });
    });

    $('#view-stats').click(function (){
        function _update(){
            var table = $(stats.exportData('html')).filter('table')[0].outerHTML;
            dialog.find('div > div').html(table.indexOf('<td') == -1 ? 
                    '<p>Данных пока нет.</p>' : table).find('table').tablesorter({
                sortList: [[0, 0]],
                textExtraction: function (e){
                    return +e.getAttribute('data-value');
                }
            });
        }

        var table = stats.exportData('html'),
            dialog = new Dialog('Статистика', [], function (){
                _update();
                return false;
            });

        _update();
        dialog.find('button').text('Обновить');
    });

    $('#export-stats').click(function (){
        function _export(type){
            var a = document.createElement('input');
            a.type = 'file';
            a.setAttribute('accept', '.' + type);
            a.setAttribute('nwsaveas', 'stats');
            a.onchange = function (){
                stats.exportData(type, a.files[0].path);
            };
            a.click();
        }

        var dialog = new Dialog('Экспорт', [
            'Выберите формат',
            '<select>\
                <option value="csv">CSV</option>\
                <option value="html">HTML</option>\
                <option value="json">JSON</option>\
                <option value="xlsx">XLSX</option>\
            </select>'
        ], function (){
            _export(localStorage.exportFormat = this.find('select').val());
        }).find('select').val(localStorage.exportFormat || 'csv');
    });

    $('#tools').click(function (){
        $('aside').toggleClass('open');
        this.classList.toggle('active');
        localStorage.tools = this.classList.contains('active');
    }).on('contextmenu', function (){
        nwWindow.showDevTools();
    });

    if (localStorage.tools == 'true'){
        $('#tools')[0].click();
    }

    $('[name="spawn"]').change(function (){
        localStorage.spawnPeriodically = spawnPeriodically = $('#spawn-periodically')[0].checked;
        localStorage.spawnFlood = spawnFlood = $('#spawn-flood')[0].checked;
    });

    if (localStorage.spawnFlood == 'true'){
        $('#spawn-flood')[0].checked = spawnFlood = true;
    } else if (localStorage.spawnPeriodically == 'false'){
        $('#spawn-off')[0].checked = true;
        spawnPeriodically = false;
    }

    $('#hide-map').click(function (){
        $('svg').toggle();
        this.classList.toggle('active');
        localStorage.hideMap = this.classList.contains('active');
    });

    if (localStorage.hideMap == 'true'){
        $('#hide-map')[0].click();
    }

    $('#show-everything').click(function (){
        showEverything = !showEverything;
        this.classList.toggle('active');
        localStorage.showEverything = this.classList.contains('active');
    });

    if (localStorage.showEverything == 'true'){
        $('#show-everything')[0].click();
    }