(function (){
    var paths = {};

    function vehicleCreated(vehicle){
    }

    function vehicleRemoved(vehicle){
        var e = paths[vehicle.meta.path.origin];
        if (!e){
            paths[vehicle.meta.path.origin] = e = {};
        }

        e = e[vehicle.meta.path.destination];
        if (!e){
            paths[vehicle.meta.path.origin][vehicle.meta.path.destination] = e = {
                count: 0,
                totalTime: 0.0,
                totalDistance: 0.0
            };
        }

        e.count++;
        e.totalTime += Date.now() - vehicle.meta.timestamp;
        e.totalDistance += vehicle.meta.path.distance;
    }

    function formatTime(ms){
        var s = Math.floor(ms / 1e3);
        var t = s % 60;
        var p = t < 10 ? '0' + t : t;
        t = Math.floor(s / 60) % 60;
        p = (t < 10 ? '0' + t : t) + ':' + p;
        t = Math.floor(s / 3600); 
        return (t < 10 ? '0' + t : t) + ':' + p;
    }

    function exportData(format, filename){
        var table = [],
            scale = 4.563323049376233e-05;

        for (var n in paths){
            for (var m in paths[n]){
                var e = paths[n][m];
                table.push({
                    from: n,
                    to: m,
                    count: e.count,
                    averageDistance: e.totalDistance * scale / e.count,
                    averageTime: e.totalTime / e.count,
                    averageSpeed: 3.6e6 * e.totalDistance * scale / e.totalTime,
                });
            }
        }

        switch (format){
            case 'csv':
                return tableToCsv(table, filename);

            case 'html':
                return tableToHtml(table, filename);

            case 'json':
                return tableToJson(table, filename);

            case 'xlsx':
                return tableToXlsx(table, filename);

            default:
                throw new Error('format not implemented: ' + format);
        }
    }

    function tableToCsv(table, filename){
        var data = '"Отправление","Назначение","Проехало машин","Расстояние","Среднее время","Средняя скорость"\r\n' + 
                table.map(function (e){
                    return '"#' + e.from + '","#' + e.to + '",' + e.count + ',"' + 
                        e.averageDistance.toFixed(2) + ' км","' + formatTime(e.averageTime) + '","' + 
                        e.averageSpeed.toFixed(0) + ' км/ч"\r\n';
                }).join('');

        if (filename != null){
            require('fs').writeFile(filename, data);
        }

        return data;
    }

    function tableToHtml(table, filename){
        var data = '<!DOCTYPE html><html><head>\
                <meta charset="utf-8"><style>table{width:100%}table th{\
                text-align:left}</style></head><body>\
                    <table><thead>\
                        <th>Отправление</th>\
                        <th>Назначение</th>\
                        <th>Проехало машин</th>\
                        <th>Расстояние</th>\
                        <th>Среднее время</th>\
                        <th>Средняя скорость</th>\
                    </thead>' + table.map(function (e){
                        return '<tr>\
                            <td data-value="' + e.from + '">#' + e.from + '</td>\
                            <td data-value="' + e.to + '">#' + e.to + '</td>\
                            <td data-value="' + e.count + '">' + e.count + '</td>\
                            <td data-value="' + e.averageDistance + '">' + e.averageDistance.toFixed(2) + ' км</td>\
                            <td data-value="' + e.averageTime + '">' + formatTime(e.averageTime) + '</td>\
                            <td data-value="' + e.averageSpeed + '">' + e.averageSpeed.toFixed(0) + ' км/ч</td>\
                        </tr>'
                    }).join('') + '</table>\
                </body></html>';

        if (filename != null){
            require('fs').writeFile(filename, data);
        }

        return data;
    }

    function tableToJson(table, filename){
        var data = JSON.stringify(table);

        if (filename != null){
            require('fs').writeFile(filename, data);
        }

        return data;
    }

    function tableToXlsx(table, filename){
        var xlsx = require('../../lib/xlsx');

        function _datenum(v, date1904) {
            if (date1904){
                v += 1462;
            }

            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
        }
         
        function _sheet(data) {
            var result = {},
                range = {
                    s: { c: 1e7, r: 1e7 },
                    e: { c: 0, r: 0 }
                };

            for (var R = 0; R != data.length; R++){
                for (var C = 0; C != data[R].length; C++){
                    if (range.s.r > R){
                        range.s.r = R;
                    }
                    if (range.s.c > C){
                        range.s.c = C;
                    }
                    if (range.e.r < R){
                        range.e.r = R;
                    }
                    if (range.e.c < C){
                        range.e.c = C;
                    }

                    var cell = { v: data[R][C] };
                    if (cell.v == null){
                        continue;
                    }
                    
                    if (typeof cell.v == 'number'){
                        cell.t = 'n';
                    } else if (typeof cell.v == 'boolean'){
                        cell.t = 'b'
                    } else if (cell.v instanceof Date) {
                        cell.t = 'n';
                        cell.z = xlsx.SSF._table[14];
                        cell.v = _datenum(cell.v);
                    } else {
                        cell.t = 's';
                    }

                    var cellRef = xlsx.utils.encode_cell({
                        c: C, r: R
                    });
                    
                    result[cellRef] = cell;
                }
            }

            if (range.s.c < 1e7){
                result['!ref'] = xlsx.utils.encode_range(range);
            }

            return result;
        }
         
        var converted = new Array(table.length + 1);
        converted[0] = [ 'Отправление', 'Назначение', 'Проехало машин', 'Расстояние', 'Среднее время', 'Средняя скорость' ];

        for (var i = 0; i < table.length; i++){
            var e = table[i];
            converted[i + 1] = [
                '#' + e.from,
                '#' + e.to,
                e.count,
                e.averageDistance.toFixed(2) + ' км',
                formatTime(e.averageTime),
                e.averageSpeed.toFixed(0) + ' км/ч',
            ];
        }

        var name = 'ИМТП-2',
            sheets = {};

        sheets[name] = _sheet(converted);

        if (filename == null){
            throw new Error('not implemented');
        }

        xlsx.writeFile({
            SheetNames: [ name ],
            Sheets: sheets
        }, filename);
    }

    var result = {
        vehicleCreated: vehicleCreated,
        vehicleRemoved: vehicleRemoved,
        exportData: exportData,
    };

    typeof module == 'object' ? (module.exports = result) : 
            (window.stats = result);
}());