(function (){
    var vector = require('../lib/vector');

    var Pathfinder = require('../lib/pathfinder').Pathfinder;

    var map,
        stats,
        vehicles = [],
        origins,
        pathsWithStations,
        lastId = 0;

    function prepareMap(){
        for (var i = 0; i < map.edges.length; i++){
            map.edges[i].vehicles = [];
        }
    }

    function collectFrom(vertice, left, array){
        var edges = vertice.edges;
        for (var i = 0; i < edges.length; i++){
            if (edges[i].toVertice == vertice){
                array.push(edges[i]);

                if (left > 1){
                    collectFrom(edges[i].fromVertice, left - 1, array);
                }
            }
        }
    }

    function preparePath(start, path){
        path.from = start;
        path.start = path.points[0];

        for (var i = 0; i < path.points.length; i++){
            var step = path.points[i],
                previous = path.points[i - 1] || start;

            step.dir = { 
                x: step.x - previous.x, y: step.y - previous.y
            };
            step.angle = vector.angleDeg(previous, step);
            step.length = vector.distance(previous, step);
            step.edge = map.edges[step.e];
            step.edge.busy = false;
            step.alternatives = [];

            if (step.edge.name == 'sub'){
                step.primary = [];

                var edges = step.edge.toVertice.edges;

                for (var j = 0; j < edges.length; j++){
                    if (edges[j] != step.edge && edges[j].to == step.edge.to){
                        step.primary.push(edges[j]);
                        collectFrom(edges[j].fromVertice, 2, step.primary);
                    }
                }
            } else {
                step.primary = null;
            }
        }

        for (var i = 0; i < path.points.length - 1; i++){
            var step = path.points[i],
                next = path.points[i + 1];

            step.factor = Math.pow((step.dir.x * next.dir.x + step.dir.y * next.dir.y) / 
                    step.length / next.length, 7);
            step.next = next;
        }

        path.points[i].factor = 1;
    }

    function setAlternatives(paths){
        function _close(a, b){
            return (a.to != b.to || a.from != b.from) &&
                    vector.distanceSqr(a.fromVertice, b.fromVertice) < 160 &&
                    vector.distanceSqr(a.toVertice, b.toVertice) < 160;
        }

        function _set(a, b){
            for (var i = 0; i < a.length; i++){
                for (var j = 0; j < b.length; j++){
                    if (_close(a[i].edge, b[j].edge)){
                        a[i].alternatives.push(b[j]);
                    }
                }
            }
        }

        for (var i = 0; i < paths.length; i++){
            var path = paths[i];

            for (var j = 0; j < paths.length; j++){
                if (j != i){
                    _set(paths[i].points, paths[j].points);
                }
            }
        }
    }

    function buildPathsByOrigins(pathfinder, from, to){
        var origins = [],
            destinations = {};

        while (from.length > 0){
            var id = from[0].number,
                points = [],
                paths = [];

            from = from.filter(function (a){
                if (a.number == id){
                    points.push(a);
                    return false;
                } else {
                    return true;
                }
            });

            for (var i = 0; i < points.length; i++){
                for (var j = 0; j < to.length; j++){
                    var path = pathfinder.find(points[i].id, to[j].id);

                    if (path != null){
                        preparePath(points[i], path);
                        path.origin = points[i].number;
                        path.destination = to[j].number;

                        var totalDistance = vector.distance(points[i], to[j]),
                            weight = totalDistance < 500 ? 0.1 : 
                                    totalDistance < 1000 ? 0.3 : 1.0;
                        paths.push({
                            path: path,
                            weight: weight
                        });

                        if (destinations[path.destination]){
                            destinations[path.destination].push(path);
                        } else {
                            destinations[path.destination] = [ path ];
                        }
                    }
                }
            }

            if (paths.length > 0){
                origins.push({
                    id: id,
                    paths: paths,
                    total: paths.reduce(function (a, b){
                        return a + b.weight;
                    }, 0.0)
                });
            }
        }

        for (var n in destinations){
            setAlternatives(destinations[n]);
        }

        return origins;
    }

    function buildPathsWithStations(pathfinder, from, stops, to){
        var pathsWithStations = [];
        for (var i = 0; i < stops.length; i++){
            for (var j = 0; j < from.length; j++){
                for (var k = 0; k < to.length; k++){
                    var a = pathfinder.find(from[j].id, stops[i].id),
                        b = pathfinder.find(stops[i].id, to[k].id);

                    if (a && b){
                        a.points[a.points.length - 1].pause = 10.0;
                        a.points.push.apply(a.points, b.points);
                        a.distance += b.distance;

                        preparePath(from[j], a);
                        a.origin = from[i].number;
                        a.destination = to[j].number;

                        a.destination = to[k].number;
                        pathsWithStations.push(a);
                    }
                }
            }
        }

        return pathsWithStations;
    }

    function buildPaths(){
        var pathfinder = new Pathfinder(map),
            from = map.vertices.filter(function (a){
                return a.number && a.edgesFrom.length == 1 && a.edges.length == 1;
            }),
            to = map.vertices.filter(function (a){
                return a.number && a.edgesFrom.length == 0 && a.edges.length == 1;
            }),
            stops = map.vertices.filter(function (a){
                return a.number && a.edgesFrom.length == 1 && a.edges.length == 2;
            });

        
        pathsWithStations = buildPathsWithStations(pathfinder, from, stops, to);
        origins = buildPathsByOrigins(pathfinder, from, to);
    }

    function create(type){
        switch (type){
            case 'bus':
                return {
                    dynamic: {
                        speed: 0.5,
                        max: 0.7,
                        acceleration: 0.005,
                        delay: 0.0,
                        breaking: 0.39,
                        distance: 16.2,
                        drafting: 1.01,
                        safe: 210.0,
                    },
                    mood: {
                        irritability: 0.0001,
                        cheerfulness: 0.048,
                        impatience: 0.0,
                        novelty: 0.0,
                    },
                    size: { 
                        x: 6.5,
                        y: 17.8 
                    },
                    meta: {
                        color: '#ff0'
                    },
                };

            case 'truck':
                return {
                    dynamic: {
                        speed: 0.8,
                        max: 1.0,
                        acceleration: 0.009,
                        delay: 0.0,
                        breaking: 0.45,
                        distance: 15.4,
                        drafting: 1.01,
                        safe: 300.0,
                    },
                    mood: {
                        irritability: 0.0002,
                        cheerfulness: 0.054,
                        impatience: 0.0,
                        novelty: 0.0,
                    },
                    size: {
                        x: 6.5,
                        y: 16.4,
                    },
                    meta: {
                        color: '#530',
                    },
                };

            default:
                return {
                    dynamic: {
                        speed: 1.3,
                        max: 1.5,
                        acceleration: 0.012,
                        delay: 0.0,
                        breaking: 0.52,
                        distance: 13.2, /* дистанция до впереди идущей машины */
                        drafting: 1.05, /* превышение максимальной скорости при движении за кем-то */
                        safe: 240.0, /* расстояние до другого автомобиля, до которого нельзя выехать со второстепенной дороги */
                    },
                    mood: {
                        irritability: 0.0003,
                        cheerfulness: 0.072,
                        impatience: 0.0,
                        novelty: 0.0,
                    },
                    size: {
                        x: 5.5,
                        y: 11.8,
                    },
                    meta: {
                        color: 'hsl(' + (Math.random() * 360 | 0) + ', 50%, 70%)',
                    },
                };
        }
    }

    function getPath(origin){
        var value = Math.random() * origin.total,
            path = 0;

        for (var i = 0; i < origin.paths.length && value > 0; i++){
            value -= origin.paths[i].weight;

            if (value < 0){
                return origin.paths[i].path;
            }
        }
    }

    function add(type, originId){
        var path;
        if (type == 'bus' && pathsWithStations.length > 0){
            path = pathsWithStations[pathsWithStations.length * Math.random() | 0];
        } else if (originId == null){
            path = getPath(origins[origins.length * Math.random() | 0]);
        } else {
            for (var i = 0; i < origins.length; i++){
                if (origins[i].id == originId){
                    path = getPath(origins[i]);
                    break;
                }
            }
        }

        if (path == null){
            return;
        }

        var next = findNextAt(path.start.edge);
        if (next && next.left > path.start.length - 20){
            return;
        }

        var vehicle = create(type);

        vehicle.meta.id = lastId++;
        vehicle.meta.type = type;
        vehicle.meta.path = path;
        vehicle.meta.timestamp = Date.now();

        vehicle.step = path.start;
        vehicle.pos = { x: path.from.x, y: path.from.y };
        vehicle.left = path.start.length;
        vehicle.angle = path.start.angle;
        vehicle.pause = null;

        vehicle.dynamic.distancePlus = vehicle.dynamic.distance * 1.9;
        vehicle.dynamic.safeSqr = Math.pow(vehicle.dynamic.safeSqr, 2);

        path.start.edge.vehicles.push(vehicle);
        vehicles.push(vehicle);

        stats.vehicleCreated(vehicle);
    }

    function init(_map, _stats){
        map = _map;
        stats = _stats;

        prepareMap();
        buildPaths();
    }

    function interpolateAngleDeg(from, to, k){
        if (k > 1){
            return to;
        } else if (k < 0){
            return from;
        } else {
            var delta = to - from,
                result = from + (delta > 180 ? delta - 360 : 
                        delta < -180 ? delta + 360 : delta) * k;
            return result > 180 ? result - 360 : 
                    result < -180 ? result + 360 : result;
        }
    }

    function findNextAt(edge, left){
        if (left === undefined){
            left = Number.POSITIVE_INFINITY;
        }

        var next = null;

        for (var i = 0; i < edge.vehicles.length; i++){
            var v = edge.vehicles[i];
            if (v.left < left && (next == null || v.left > next.left)){
                next = v;
            }
        }

        return next;
    }

    function findNext(step, left, distanceRef){
        distanceRef[0] = left;

        for (var i = 0; i < 2; i++){
            var result = findNextAt(step.edge, i == 0 ? left : Number.POSITIVE_INFINITY);

            if (result){
                distanceRef[0] -= result.left;
                return result;
            } else if (step.next){
                step = step.next;
                distanceRef[0] += step.length;
            } else {
                break;
            }
        }

        return null;
    }

    function findPrimary(edges){
        for (var i = 0; i < edges.length; i++){
            var result = findNextAt(edges[i]);

            if (result){
                return result;
            }
        }
    }

    function changeStep(vehicle, step){
        if (vehicle.step.primary){
            vehicle.step.primary[0].busy = false;
        }

        var edgeVehicles = vehicle.step.edge.vehicles;
        edgeVehicles[edgeVehicles.indexOf(vehicle)] = edgeVehicles[edgeVehicles.length - 1];
        edgeVehicles.length--;

        vehicle.step = step;
        if (step){
            step.edge.vehicles.push(vehicle);
            vehicle.angle = step.angle;
        }
    }

    function remove(pos){
        stats.vehicleRemoved(vehicles[pos]);
        vehicles[pos--] = vehicles[vehicles.length - 1];
        vehicles.length--;
    }

    function update(){
        var distance = [ 0 ],
            now = Date.now();

        for (var i = 0; i < vehicles.length; i++){
            var vehicle = vehicles[i],
                step = vehicle.step,
                dynamic = vehicle.dynamic,
                mood = vehicle.mood,
                sizeY = vehicle.size.y;

            /* до завершения текущего участка */
            var dir = {
                x: step.x - vehicle.pos.x,
                y: step.y - vehicle.pos.y,
            };
            vehicle.left = vector.length(dir);

            /* целевая скорость */
            var targetSpeed = dynamic.max;

            if (step.edge.busy && vehicle.left > sizeY * .5 && vehicle.left < dynamic.distancePlus ||
                step.next && step.next.edge.busy && step.next.length + vehicle.left < dynamic.distancePlus){
                /* резко тормозим, если впереди кто-то выезжает со второстепенной дороги */
                targetSpeed = 0;
            } else {
                /* тормозим */
                if (vehicle.pause != null){
                    if (vehicle.pause < now){
                        vehicle.pause = null;
                    }

                    targetSpeed *= (vehicle.left - sizeY) / step.length;
                } 

                /* если впереди кто-то едет, подстраиваемся и держим дистанцию */
                var nextVehicle = findNext(step, vehicle.left, distance);
                if (nextVehicle){
                    var factor = (distance[0] - dynamic.distance - nextVehicle.size.y / 2) / 20;
                    if (factor > dynamic.drafting){
                        factor = dynamic.drafting;
                    }

                    /* не даём откатываться назад */
                    if (factor < -0.03){
                        factor = -0.03;
                    }

                    targetSpeed = Math.min(
                        targetSpeed * dynamic.drafting,
                        nextVehicle.dynamic.speed + factor
                    );

                    /* если он мешает, пытаемся обогнать */
                    var alternatives = step.alternatives;
                    if (alternatives.length > 0 && factor < .1 && mood.novelty < .02 && targetSpeed < dynamic.max * .7 &&
                            mood.impatience > .02 && vehicle.left > sizeY && step.length - vehicle.left > sizeY){
                        for (var j = 0; j < alternatives.length; j++){
                            var candidate = alternatives[j],
                                temp = findNextAt(candidate.edge, vehicle.left + sizeY + dynamic.distance);
                            if (temp == null || temp.left < vehicle.left - dynamic.distance * (3.6 - 1.2 * mood.impatience)){
                                vehicle.pos.x += ((candidate.edge.fromVertice.x - step.edge.fromVertice.x) +
                                        (candidate.edge.toVertice.x - step.edge.toVertice.x)) / 2;
                                vehicle.pos.y += ((candidate.edge.fromVertice.y - step.edge.fromVertice.y) +
                                        (candidate.edge.toVertice.y - step.edge.toVertice.y)) / 2;
                                changeStep(vehicle, candidate);
                                mood.novelty = 1.0;
                                continue;
                            }
                        }
                    }
                }

                /* при выезде со второстепенной дороги проверяем, не едет ли кто */
                if (step.primary && vehicle.left < dynamic.distancePlus && vehicle.left > sizeY){
                    var primary = findPrimary(step.primary);
                    if (primary && vehicle.left > sizeY && (mood.impatience < .07 || primary.dynamic.speed > .3) &&
                            vector.distanceSqr(vehicle.pos, primary.pos) < dynamic.safeSqr * (1.0 - .7 * mood.impatience)){
                        var fixSpeed = (vehicle.left - dynamic.distance) / 5;
                        if (fixSpeed < targetSpeed){
                            targetSpeed = fixSpeed < 0 ? fixSpeed : 0;
                        }

                        step.primary[0].busy = false;
                    } else {
                        /* если решаем выехать, помечаем ребро, чтобы, если кто-то и поедет, он притормозил */
                        step.primary[0].busy = true;
                        dynamic.delay = 0.0;
                    }
                }

                /* скидываем скорость перед входом в поворот */
                var factor = vehicle.left / 100 + .1 + .2 * mood.impatience;
                if (factor < 1){
                    targetSpeed *= step.factor * (1 - factor) + factor;
                }
            }

            /* приближаем скорость к целевой, тормозя или разгоняясь */
            if (targetSpeed > dynamic.speed){
                if (dynamic.delay > 0.0){
                    dynamic.delay -= mood.cheerfulness;
                } else {
                    dynamic.speed += dynamic.acceleration;
                }
            } else {
                dynamic.speed += (targetSpeed - dynamic.speed) * dynamic.breaking;
            }

            if (targetSpeed < dynamic.speed + .05 && dynamic.speed < 0.05){
                dynamic.delay += (1.0 - dynamic.delay) * 0.2;
            }

            /* настроение */
            if (dynamic.speed > dynamic.max * .6){
                mood.impatience *= 1.0 - 0.1 * dynamic.speed / dynamic.max;
            } else {
                mood.impatience += (1.0 - mood.impatience) * mood.irritability *
                        (1.0 - dynamic.speed / dynamic.max);
            }
            mood.novelty *= 0.95;

            if (vehicle.left > dynamic.speed){
                /* едем */
                vehicle.pos.x += dir.x * dynamic.speed / vehicle.left;
                vehicle.pos.y += dir.y * dynamic.speed / vehicle.left;
            } else if (step.next == null){
                /* всё проехали, удаляемся */
                remove(i);
            } else {
                /* переходим на следующее ребро */
                changeStep(vehicle, step.next);

                if (vehicle.step.pause){
                    vehicle.pause = now + vehicle.step.pause * 1e3;
                }
            }
        }
    }

    var result = {
        init: init,
        add: add,
        update: update,

        get origins(){
            return origins;
        },

        get list(){
            return vehicles;
        },

        get count(){
            return vehicles.length;
        }
    };

    typeof module == 'object' ? (module.exports = result) : 
            (window.vehicles = result);
}());