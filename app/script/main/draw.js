(function (document, $){
    var svg = $('svg'),
        width = document.body.offsetWidth,
        height = document.body.offsetHeight,
        group;

    var dx, dy, scale;
    var pan;

    var mapBackground, mapBackgroundPos, mapBackgroundSize;
    $('canvas, svg').attr({ width: width, height: height });
    $(window).on('resize', function (){
        width = document.body.offsetWidth;
        height = document.body.offsetHeight;
        $('canvas, svg').attr({ width: width, height: height });
    });

    var bridges = [];

    function create(tagName){
        return document.createElementNS('http://www.w3.org/2000/svg', tagName);
    }

    function pageToMap(x, y){
        var b = svg.offset();
        return { x: (x - b.left - dx) / scale, y: (y - b.top - dy) / scale };
    }

    function updateGroupTransform(){
        group.attr({
            transform: [ 'translate(', dx, dy, ')', 'scale(', scale, scale, ')' ].join(' ')
        }).css({
            strokeWidth: 2 / scale,
        });

        if (mapBackground){
            mapBackground.style.left = (dx + mapBackgroundPos.x * scale) + 'px';
            mapBackground.style.top = (dy + mapBackgroundPos.y * scale) + 'px';
            mapBackground.style.width = mapBackgroundSize.x * scale + 'px';
            mapBackground.style.height = mapBackgroundSize.y * scale + 'px';
        }
    }

    function changeScale(d, px, py){
        var ns;
        if (typeof d == 'string'){
            var delta = 1 + Math.abs(+d);
            ns = scale * (+d < 0 ? 1 / delta : delta);
        } else {
            ns = d;
        }

        px = px != null ? px : svg[0].offsetWidth / 2;
        py = py != null ? py : svg[0].offsetHeight / 2;

        dx = px - (px - dx) * ns / scale;
        dy = py - (py - dy) * ns / scale;

        scale = ns;
        updateGroupTransform();
    }

    function changePosition(x, y){
        dx = typeof x == 'string' ? dx + +x : x;
        dy = typeof y == 'string' ? dy + +y : y;

        updateGroupTransform();
    }

    function centerMap(map){
        var padding = 10;

        var mx = (width - padding * 2) / map.boundingBox.width,
            my = (height - padding * 2) / map.boundingBox.height;

        scale = Math.min(mx, my);
        dx = width / 2 - (map.boundingBox.left + map.boundingBox.width / 2) * scale;
        dy = height / 2 - (map.boundingBox.top + map.boundingBox.height / 2) * scale;

        updateGroupTransform();
    }

    function drawMap(map){
        bridges = [];
        mapBackground = null;

        svg.html('');
        $(create('style')).html('line{stroke:#555;stroke-linecap:round}path{fill:#555}\
                text{fill:#f44;stroke:#000;stroke-width:.5;font-size:18;text-anchor:middle}').appendTo(svg);

        $(create('marker')).attr({
            id: 'triangle',
            viewBox: '0 0 10 10',
            refX: '10', refY: '5',
            markerUnits: 'strokeWidth',
            markerWidth: '4', markerHeight: '3',
            orient: 'auto',
        }).appendTo(svg).append(
            $(create('path')).attr({
                d: 'M 0 0 L 10 5 L 0 10 z',
            })
        );

        group = $(create('g')).appendTo(svg);

        var bridgeEdges = {};
        for (var i = 0; i < map.edges.length; i++){
            var e = map.edges[i];
            if (e.name && e.name.indexOf('bridge') == 0){
                if (bridgeEdges[e.name]){
                    bridgeEdges[e.name].push(e);
                } else {
                    bridgeEdges[e.name] = [ e ];
                }
            } else {
                $(create('line')).attr({
                    x1: e.fromVertice.x, y1: e.fromVertice.y, 
                    x2: e.toVertice.x, y2: e.toVertice.y,
                }).attr({
                    'marker-end': e.oneway ? 'url(#triangle)' : null
                }).appendTo(group);
            }
        }

        for (var i = 0; i < map.vertices.length; i++){
            var v = map.vertices[i];

            if (v.image){
                var m = v.image.split('/'),
                    f = m.slice(0, -2).join('/');
                if (f.indexOf('/') == -1){
                    f = 'file:///' + require('path').dirname(process.execPath).replace(/\\/g, '/') + '/' + f;
                }
                mapBackground = $('img').attr({ 
                    width: m[m.length - 2], height: m[m.length - 1], src: f
                })[0];

                mapBackgroundPos = { x: v.x, y: v.y };
                mapBackgroundSize = { x: m[m.length - 2], y: m[m.length - 1] };
            }

            if (v.number != null){
                $(create('text')).attr({
                    x: v.x,
                    y: v.y + 10,
                }).text(v.number).appendTo(group);
            }
        }

        for (var n in bridgeEdges){
            var array = bridgeEdges[n];
            console.assert(array.length == 2);

            var a = array[0].fromVertice,
                b = array[0].toVertice,
                c = array[1].toVertice;

            bridges.push({
                ax: a.x - b.x,
                ay: a.y - b.y,
                bx: a.x - c.x,
                by: a.y - c.y,
                cx: c.x - a.x + b.x,
                cy: c.y - a.y + b.y,
            });
        }
    }

    function setupEvents(){
        var spacePressed, spacePanned, px, py;
        $(document.body).mousedown(function (e){
            if (e.which == 2 || spacePressed){
                if (spacePressed && !spacePanned){
                    document.body.classList.add('hand-cursor');
                    spacePanned = true;
                }

                pan = true;
                return false;
            }
        }).mouseup(function (e){
            setTimeout(function (){
                pan = false;
            }, 10);
        }).mousemove(function (e){
            var x = e.pageX - px, y = e.pageY - py;
            if (pan && (x || y)){
                changePosition('' + x, '' + y);
            }
            px = e.pageX, py = e.pageY;
        }).mouseleave(function (){
            pan = false;
        }).keydown(function (e){
            if (/^(INPUT|TEXTAREA|SELECT)$/.test(e.target.tagName)){
                return;
            }
            if (e.keyCode == 32){
                if (!spacePressed){
                    spacePressed = true;
                    spacePanned = false;
                }

                return false;
            } else if (e.keyCode == 27){
                $('dialog').remove();
                return false;
            }
        }).keyup(function (e){
            if (/^(INPUT|TEXTAREA|SELECT)$/.test(e.target.tagName)){
                return;
            }
            if (e.keyCode == 32){
                document.body.classList.remove('hand-cursor');
                spacePressed = false;
                return !spacePanned;
            }
        })[0].addEventListener('mousewheel', function (e, d){
            if (e.target.tagName == 'CANVAS'){
                var d = e.wheelDelta < 0 ? '-0.3' : e.wheelDelta > 0 ? '+0.3' : 0;
                if (d){
                    var o = svg.offset();
                    changeScale(d, e.pageX - o.left, e.pageY - o.top);
                }
            }
        }, false);
    }

    var canvas = $('canvas')[0],
        context = canvas.getContext('2d');

    function drawVehicles(vehicles, showEverything){
        function _draw(vehicle){
            if (vehicle.posDraw){
                vehicle.posDraw.x += (vehicle.pos.x - vehicle.posDraw.x) / 4;
                vehicle.posDraw.y += (vehicle.pos.y - vehicle.posDraw.y) / 4;

                var delta = vehicle.angle - vehicle.angleDraw,
                    angleDraw = vehicle.angleDraw + (delta > 180 ? 
                            delta - 360 : delta < -180 ? delta + 360 : delta) / 4;
                vehicle.angleDraw = angleDraw > 180 ? angleDraw - 360 :
                        angleDraw < -180 ? angleDraw + 360 : angleDraw;
            } else {
                vehicle.posDraw = { x: vehicle.pos.x, y: vehicle.pos.y };
                vehicle.angleDraw = vehicle.angle;
            }

            context.fillStyle = vehicle.meta.color;
            context.save();
            context.translate(vehicle.posDraw.x, vehicle.posDraw.y);
            context.rotate(vehicle.angleDraw * 0.017453292519943295); /* deg to rad */
            context.fillRect(-vehicle.size.x / 2, -vehicle.size.y / 2, vehicle.size.x, vehicle.size.y);
            context.restore();
        }

        context.clearRect(0, 0, width, height);

        context.save();
        context.translate(dx, dy);
        context.scale(scale, scale);

        for (var i = 0; i < vehicles.length; i++){
            var vehicle = vehicles[i];
            if (vehicle.step.edge.name != 'over'){
                _draw(vehicle);
            }
        }

        if (!showEverything){
            for (var i = 0; i < bridges.length; i++){
                var bridge = bridges[i];
                context.save();
                context.transform(bridge.ax, bridge.ay, bridge.bx, bridge.by, bridge.cx, bridge.cy);
                context.clearRect(0, 0, 1, 1);
                context.restore();
            }
        }

        for (var i = 0; i < vehicles.length; i++){
            var vehicle = vehicles[i];
            if (vehicle.step.edge.name == 'over'){
                _draw(vehicle);
            }
        }

        context.restore();
    }

    function formatTime(ms){
        var s = Math.floor(ms / 1e3);
        var t = s % 60;
        var p = t < 10 ? '0' + t : t;
        t = Math.floor(s / 60) % 60;
        p = (t < 10 ? '0' + t : t) + ':' + p;
        t = Math.floor(s / 3600); 
        return (t < 10 ? '0' + t : t) + ':' + p;
    }

    function drawStats(totalVehicles, passedTime, framesPerSecond, calculationTime){
        $('#stats').html(
            'Всего машин: ' + totalVehicles + '\n' +
            'Прошло: ' + formatTime(passedTime) + '\n' +
            'FPS: ' + framesPerSecond.toFixed(0) + '\n' +
            'Расчёт: ' + calculationTime.toFixed(2) + 'мс'
        );
    }

    function drawStatsVehicle(vehicle, pause){
        $('#stats').html(
            '<span style="background-color:' + vehicle.meta.color + '"></span>' +
            'Автомобиль: #' + vehicle.meta.id + '\n' +
            'Прошло: ' + formatTime((pause || Date.now()) - vehicle.meta.timestamp) + '\n' +
            'Путь: #' + vehicle.meta.path.origin + ' → #' + vehicle.meta.path.destination + '\n' +
            (vehicle.type ? 'Тип: ' + vehicle.meta.type + '\n' : '') +
            'Скорость: ' + (vehicle.dynamic.speed * 62).toFixed(0) + 'км/ч\n' +
            'Нетерпение: ' + vehicle.mood.impatience.toFixed(2) + '\n' +
            'Новизна: ' + vehicle.mood.novelty.toFixed(2) + '\n' +
            'Задержка: ' + vehicle.dynamic.delay.toFixed(2) + '\n' +
            'Размер: ' + (vehicle.size.x / 3).toFixed(2) + '×' + (vehicle.size.y / 3).toFixed(2) + 'м'
        );
    }

    var result = {
        drawMap: drawMap,
        centerMap: centerMap,
        setupEvents: setupEvents,
        drawVehicles: drawVehicles,
        drawStats: drawStats,
        drawStatsVehicle: drawStatsVehicle,
        pageToMap: pageToMap,
    };

    typeof module == 'object' ? (module.exports = result) : 
            (window.draw = result);
}(window.document, window.$));