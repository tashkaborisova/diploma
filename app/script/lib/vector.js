module.exports = {
    angle: function (a, b){
        return Math.atan2(b.x - a.x, a.y - b.y);
    },
    angleDeg: function (a, b){
        return Math.atan2(b.x - a.x, a.y - b.y) * 180 / Math.PI;
    },
    length: function (v){
        return Math.sqrt(v.x * v.x + v.y * v.y);
    },
    lengthSqr: function (v){
        return v.x * v.x + v.y * v.y;
    },
    distance: function (a, b){
        var dx = a.x - b.x,
            dy = a.y - b.y;
        return Math.sqrt(dx * dx + dy * dy);
    },
    distanceSqr: function (a, b){
        var dx = a.x - b.x,
            dy = a.y - b.y;
        return dx * dx + dy * dy;
    },
    normalize: function (v){
        var l = Math.sqrt(v.x * v.x + v.y * v.y);
        v.x /= l;
        v.y /= l;
    },
    multiply: function (v, m){
        v.x *= m;
        v.y *= m;
    }
};