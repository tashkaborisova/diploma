(function (){
    function _connect() {
        var vertices = this.vertices,
            edges = this.edges;

        for (var i = 0; i < vertices.length; i ++){
            var v = vertices[i];

            v.id = i;
            v.edges = [];
            v.edgesFrom = [];
        }

        for (var i = 0; i < edges.length; i ++){
            var e = edges[i];

            e.id = i;

            e.fromVertice = vertices[e.from];
            e.toVertice = vertices[e.to];

            e.fromVertice.edges.push(e);
            e.toVertice.edges.push(e);

            e.fromVertice.edgesFrom.push(e);
            if (!e.oneway){
                e.toVertice.edgesFrom.push(e);
            }
        }
    }

    var Map = function(vertices, edges) {
        this.vertices = vertices || [];
        this.edges = edges || [];

        this.abc = false;
        this.length = false;

        _connect.call(this);
    };

    Map.prototype.__defineGetter__('boundingBox', function (x, y) {
        var vertices = this.vertices;

        if (vertices.length){
            var minX = vertices[0].x, minY = vertices[0].y,
                maxX = vertices[0].x, maxY = vertices[0].y;

            for (var i = 1; i < vertices.length; i ++){
                if (vertices[i].x < minX){ minX = vertices[i].x; }
                if (vertices[i].y < minY){ minY = vertices[i].y; }
                if (vertices[i].x > maxX){ maxX = vertices[i].x; }
                if (vertices[i].y > maxY){ maxY = vertices[i].y; }
            }

            return this.boundingBox = {
                left: minX, top: minY,
                right: maxX, bottom: maxY,
                width: maxX - minX, height: maxY - minY
            }
        } else {
            return this.boundingBox = {
                left: 0, top: 0,
                right: 0, bottom: 0,
                width: 0, height: 0
            }
        }
    });

    Map.prototype.addVertice = function (x, y) {
        var added = { id: 0, x: x, y: y, edges: [], edgesFrom: [] },
            id = this.vertices.push(added);

        this.abc = false;
        this.length = false;
        delete this.boundingBox;

        added.id = id - 1;
        return added;
    };

    Map.prototype.addEdge = function (fromId, toId) {
        if (fromId == toId){
            throw new Exception("edge cannot be created: same ids");
        }

        var fromVertice = this.vertices[fromId],
            toVertice = this.vertices[toId],
            edges = fromVertice.edges;

        for (var i = 0; i < edges.length; i++){
            if (edges[i].fromVertice == toVertice || edges[i].toVertice == toVertice){
                return null;
            }
        }

        var added = { id: 0, from: fromId, to: toId, fromVertice: fromVertice, toVertice: toVertice },
            id = this.edges.push(added);

        fromVertice.edges.push(added);
        toVertice.edges.push(added);

        fromVertice.edgesFrom.push(added);
        toVertice.edgesFrom.push(added);

        this.abc = false;
        this.length = false;
        delete this.boundingBox;

        added.id = id - 1;
        return added;
    };

    Map.prototype.prepareAddresses = function () {
        var result = {};

        function address(name, number, pos){
            name = name.toLowerCase();
            if (!result.hasOwnProperty(name)){
                result[name] = {};
            }
            result[name][number] = pos;
        }

        for (var i = 0; i < this.vertices.length; i++){
            var v = this.vertices[i];

            if (v.number){
                var a = this.nearest(v, function (a){
                    if (a == v){
                        return false;
                    } else if (a.edges){
                        for (var i = 0; i < a.edges.length; i++){
                            if (a.edges[i].name){
                                return true;
                            }
                        }

                        return false;
                    } else {
                        return a.name;
                    }
                });

                if (a.verticeId){
                    var e = this.vertices[a.verticeId].edges;
                    for (var j = 0; j < e.length; j++){
                        if (e[j].name){
                            address(e[j].name, v.number);
                        }
                    }
                } else {
                    address(this.edges[a.edgeId].name, v.number, { x: v.x, y: v.y });
                }
            }
        }

        this.addresses = result;
        this.addressesPrepared = true;
    };

    Map.prototype.calculateLength = function () {
        for (var i = 0; i < this.edges.length; i++){
            var e = this.edges[i],
                x1 = e.fromVertice.x,
                y1 = e.fromVertice.y,
                x2 = e.toVertice.x,
                y2 = e.toVertice.y;

            e.length = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        }

        this.length = true;
    };

    Map.prototype.calculateAbc = function () {
        for (var i = 0; i < this.edges.length; i++){
            var e = this.edges[i],
                x1 = e.fromVertice.x,
                y1 = e.fromVertice.y,
                x2 = e.toVertice.x,
                y2 = e.toVertice.y;

            if (x1 == x2){
                e.a = e.x1;
                e.xx = true;
            } else if (y1 == y2){
                e.a = e.y1;
                e.yy = true;
            } else {
                e.a = y1 - y2;
                e.b = x2 - x1;
                e.c = (x1 - x2) * y1 + (y2 - y1) * x1;
                e.ac = e.a * e.c;
                e.bc = e.b * e.c;
                e.aabb = e.a * e.a + e.b * e.b;
            }

            e.bb = {
                left: Math.min(x1, x2) - .01,
                right: Math.max(x1, x2) + .01,
                top: Math.min(y1, y2) - .01,
                bottom: Math.max(y1, y2) + .01
            };
        }

        this.abc = true;
    };

    Map.prototype.near = function (a, b) {
        return Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) < 3;
    };

    Map.prototype.nearest = function (pos, filter) {
        console.assert(this.vertices.length > 0);

        if (!this.abc){
            this.calculateAbc();
        }

        var result = {
            x: 0, y: 0,
            edgeId: null, verticeId: null,
            distance: Number.POSITIVE_INFINITY 
        };

        for (var i = 0; i < this.edges.length; i++){
            var e = this.edges[i];

            if (filter == null || filter(e)){
                var x, y;

                if (e.xx){
                    x = e.a;
                    y = pos.y;
                } else if (e.yy){
                    x = pos.x;
                    y = e.a;
                } else {
                    x = (e.b * ( e.b * pos.x - e.a * pos.y) - e.ac) / e.aabb;
                    y = (e.a * (-e.b * pos.x + e.a * pos.y) - e.bc) / e.aabb;
                }

                if (x > e.bb.left && x < e.bb.right && 
                        y > e.bb.top && y < e.bb.bottom){
                    var d = Math.pow(x - pos.x, 2) + Math.pow(y - pos.y, 2);

                    if (d <= result.distance){
                        result.x = x;
                        result.y = y;
                        result.edgeId = e.id;
                        result.distance = d;
                    }
                }
            }
        }

        for (var i = 0; i < this.vertices.length; i++){
            var v = this.vertices[i];

            if ((filter == null || filter(v)) && v.edges.length > 0){
                var d = Math.pow(v.x - pos.x, 2) + Math.pow(v.y - pos.y, 2);

                if (d <= result.distance){
                    result.x = v.x;
                    result.y = v.y;
                    result.edgeId = null;
                    result.verticeId = v.id;
                    result.distance = d;
                }
            }
        }

        result.distance = Math.sqrt(result.distance);
        return result;
    };

    Map.prototype.byAddress = function (address) {
        if (!this.addressesPrepared){
            this.prepareAddresses();
        }

        function byNameNumber(name, number){
            name = name.toLowerCase();

            if (this.addresses.hasOwnProperty(name)){
                var street = this.addresses[name],
                    house = street[number] || street[number | 0];

                if (house){
                    return house;
                }

                /* если не нашли, попробуем перебрать строения */
                for (var n in street){
                    if (n | 0 == number){
                        return street[n];
                    }
                }
            }

            console.warn("address not found:", name, number);
            return null;
        }

        var name, number;

        try {
            name = address.match(/(?:^|[^а-яё\w])(\d*[а-яёa-z]+[\d+а-яёa-z]*)(?![а-яё\w])/i)[1];
            number = address.match(/(?:^|[^а-яё\w])(\d+(\.\d*)?)(?![а-яё\w])/i)[1];
        } catch (e){
            throw new Error('Invalid format');
        }

        return byNameNumber.call(this, name, number);
    };

    Map.prototype.toString = function () {
        return '[object Map]';
    };

    Map.prototype.serialize = function () {
        var vertices = [],
            edges = [];

        for (var i = 0; i < this.vertices.length; i++){
            var vertice = this.vertices[i];
            if (vertice){
                var obj = {
                    oldId: i,
                    empty: vertice.edges.length == 0,
                    x: vertice.x,
                    y: vertice.y
                };

                if (vertice.number != null){
                    obj.number = vertice.number;
                }

                if (vertice.image != null){
                    obj.image = vertice.image;
                }

                vertices.push(obj);
            }
        }

        /* переносим вершины без рёбер в конец */
        vertices.sort(function (a, b){
            return a.empty ? 1 : -1;
        });

        /* переназначаем идентификаторы вершин */
        var reId = {};
        for (var i = 0; i < vertices.length; i++){
            reId[vertices[i].oldId] = i;
            delete vertices[i].oldId;
            delete vertices[i].empty;
        }

        for (var i = 0; i < this.edges.length; i++){
            var temp = this.edges[i],
                obj;

            if (temp && temp.from != temp.to){
                edges.push(obj = { 
                    from: reId[temp.from],
                    to: reId[temp.to],
                    name: temp.name,
                });

                if (temp.name != null){
                    obj.name = temp.name;
                }

                if (temp.oneway == true){
                    obj.oneway = temp.oneway;
                }
            }
        }

        return JSON.stringify({
            vertices: vertices,
            edges: edges
        });
    };

    Map.deserialize = function (data) {
        var parsed = typeof data == 'object' && data.vertices && data.edges ? 
                data : JSON.parse(data);
        return new Map(parsed.vertices, parsed.edges);
    };

    Map.createEmpty = function (data) {
        return new Map([], []);
    };

    Map.createRandom = function (data) {
        var map = new Map([], []);

        for (var i = 0; i < 10; i ++){
            map.addVertice(Math.random() * 1e3, Math.random() * 1e3);
        }

        for (var i = 0; i < 20; i ++){
            var f = i % map.vertices.length;

            do {
                var t = Math.random() * map.vertices.length | 0;
            } while (t == f);

            map.addEdge(f, t);
        }

        return map;
    };

    (typeof module == 'object' ? module.exports : window).Map = Map;
}());