/* Алгоритм А* */

/* эвристическая ф-ия */
function _h(a, b){
    return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}

function _prepare(){
    this.map.calculateLength();

    var vs = this.map.vertices;
    this.vv = [];
    for (var i = 0; i < vs.length; i++){
        var v = vs[i];

        if (v.edges.length > 0){
            console.assert(i == v.id);

            var n = new Array(v.edgesFrom.length);

            this.vv[i] = {
                i: i, x: v.x, y: v.y, n: n, /* id, x, y, соседи */
                l: 0, h: 0, f: null, e: -1, p: false, /* метка, эвристическая оценка, исходная вершина, пройдена ли */
            };

            for (var j = 0; j < v.edgesFrom.length; j++){
                var e = v.edgesFrom[j],
                    o = e.to == i ? e.fromVertice : e.toVertice;
                n[j] = {
                    i: o.id,
                    w: e.length,
                    e: e.id,
                };
            }
        }
    }
}

function _find(si, ei){
    var vv = this.vv,
        ev = vv[ei];

    /* сбрасываем состояния */
    for (var i = 0; i < vv.length; i++){
        var v = vv[i];
        if (v){
            v.l = Number.POSITIVE_INFINITY;
            v.h = _h(ev, v);
            v.f = null;
            v.p = false;
        }
    }

    /* стартовая вершина */
    vv[si].l = 0;

    while (true){
        /* находим необработанную вершину с минимальной меткой */
        var ml = Number.POSITIVE_INFINITY,
            mi = -1;

        for (var i = 0; i < vv.length; i++){
            var v = vv[i];
            if (v && !v.p && v.l < ml){
                ml = v.l;
                mi = i;
            }
        }

        /* если такой нет, пути не существует */
        if (mi == -1){
            return null;
        }

        /* если найденная - та, в которую и строили путь, путь найден */
        if (mi == ei){
            var re = [],
                ci = mi;

            while (vv[ci].f != null){
                re.push({ x: vv[ci].x, y: vv[ci].y, e: vv[ci].e });
                ci = vv[ci].f;
            }

            var rr = new Array(re.length);

            for (var i = 0; i < re.length; i++){
                rr[re.length - 1 - i] = re[i];
            }

            return {
                distance: ml,
                points: rr
            };
        }

        /* иначе перебираем соседей */
        var mv = vv[mi];
        for (var i = 0; i < mv.n.length; i++){
            var ne = mv.n[i],
                nv = vv[ne.i],
                nd = ml + ne.w + nv.h;
            if (nd < nv.l){
                nv.l = nd;
                nv.f = mi;
                nv.e = ne.e;
            }
        }

        /* вершина обработана */
        mv.p = true;
    }
}

var Pathfinder = function(map) {
    this.map = map;
    _prepare.call(this);
};

Pathfinder.prototype.find = function (from, to) {
    return _find.call(this, from, to);
};

(typeof module == 'object' ? module.exports : window).Pathfinder = Pathfinder;